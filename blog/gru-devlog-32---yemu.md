GRU Devlog 32 - yemu

This 2 weeks I was coding yemu. I added handling for flags, so now after any operation ZF and
NF flags changed to correct values. Also I added opcodes for CF manipulations, so carry flag
can be set and unset. Also I was fixing and detalizing specs, because I needed it to implement
other commands. I added logical instructions - AND, OR, NOT, XOR.

This week I was mostly researching about something that can be interesting, but don't have
enough information for a project yet. It is pretty complicated sphere, so I don't know when
I will do something interesting there.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, yemu, ocpu 
