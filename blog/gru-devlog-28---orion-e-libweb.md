GRU Devlog 28 - orion, E, LibWeb

I think this weeks were productive. Last week I have accessed initrd on orion and printed
tar archive files. Currently VFS is needed to be implemented and for that I need kmalloc.

Also several more functions were added to EGG E - set background color, drawing rectangles,
lines, circles and maybe some other ones. Currently I don't know how it will be made into
real display server, but I hope that we will see.

I think the coolest project I was working this days is LibWeb. It is going to be library 
where web things will be implemented. Currently I am working on HTML tokenizer and parser.
It already can tokenize and parse very simple page with head and body, but it is already 
very cool, because i started it just several days ago. Also I would like to implement CSS
and maybe JavaScript (ECMAScript) there.

Also I thought about some other ideas - LibImage and LibJSON. I think after HTML it won't
be very hard to parse them, and they could help us in future when we will be making more
GUI things.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, orion, egg, e, libweb
