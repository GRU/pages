GRU Devlog 37 - olibc

This time I was working on olibc. I rewritten Makefile and restrustured the project a bit.
I have added some macros to sys headers, so olibc is on path of beign able to compile itself.

Currently I am working on adding other useful headers. I think we can add POSIX headers 
to it, so it will have implemented not only C11 specifications, but also some more 
UNIX-related things.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, olibc
