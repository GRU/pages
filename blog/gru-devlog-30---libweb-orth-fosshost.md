GRU Devlog 30 - libweb, orth, fosshost

Last week I found why title tag wasn't working - I haven't implemented one of cases, so it is
failing on them. I am still trying to make it working.

This week I started to make own forth implementation called orth. Currently it can do very little
amount of things: tokenize input, get numbers. Trying to implement token stack right now.

Also, I have received email from fosshost. So they accepted our application and requested details
about what VM we need. We decided to go with x86\_64 CPU, 5G RAM and 50G disk. 
I think everything should be fine.

I am a bit lazy last few weeks, but I guess it is fine and I will get back on track soon.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, libweb, orth, fosshost 
