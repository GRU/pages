GRU Devlog 33 - yemu, txtutils

This 2 weeks I spend implementing new yemu features. I implememented CMP ocpu instruction 
and added very basic stack implementation and some instructions for it. I will need to 
add more description about stack and stack manipulation instructions to ocpu specifications.

Also I started coding txtutils. I am planning it for text manipulation programs (grep, sed,
awk). Started to develop grep utility, it already can find text in input, but not supports 
some "regex commands" that I would like it to support. Also I want to add support for 
multiple files there.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, yemu, txtutils
