GRU Devlog 40 - deertree

Sorry for not posting last devlog, I haven't done anything and wanted to take a break.

This time, I was working on deertree and fixed some things on webpage. I published deertree
specifications. They are not done yet, but publishing them on git might help us to keep
progress.

As you can see, devlogs are getting little. I am working more on my other (non-GRU) projects nowadays.
New school year started and we have busy timetable. I want to focus more on some one (GRU) project
(currently thinking that it will be deertree), but take break with devlogs for unknown amount
of time. 

I want to hear some feedback. Email me (or xmpp) on [g1n@hextilde.xyz](mailto:g1n@hextilde.xyz),
join #gru channel on tilde.chat/libera.chat/teapot.chat

tags: gru, deertree
