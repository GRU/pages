GRU Devlog 25 - occ, oircd

So I decided to make this devlog every two weeks. Why? Because I joined ircnow and can be a bit busy
because of school.

Previous week I was trying to make add working in occ. I haven't done it, because I didn't understood
how to make it in proper abstract syntax tree. I don't want publish it until I will make it done.

In the end of that week I joined ircnow and got a lot of cool admin expirience and I hope it will
help me with GRU. Now I am part of the team, but for more info check post on my own blog.

This week I started developing own irc daemon. I have own server now, so i test it there. Currently it
is able to handle simple NICK and USER commands. I tryed to setup PRIVMSG, but got it working only with one user
(when privmsg to someone, user "server" replies to you). Currently trying to make it in proper way (maybe will use
dictionaries, I don't know yet).

Also maybe I will move our xmpp muc or any other things to my own server.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, occ, oircd
