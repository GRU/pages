GRU Devlog 38 - olibc

This time I was also working on olibc. I have added some datastructures, so we won't
depend on builtin system headers. 

Added open/read/write/close functions to unistd.h and fcntl.h, so we won't need to rewrite
other headers that were depending on liblinux's ones for porting. Also added abs function to stdlib.h.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, olibc
