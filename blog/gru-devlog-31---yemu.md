GRU Devlog 31 - yemu 

This week I restructured yemu and it’s Makefile. I think that our projects will need some structure “standard” to not complicate them.

I was working on adding new instructions to ocpu. I added ADC, SUB, MUL, DIV. Also some duplicated code was removed.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, yemu, ocpu
